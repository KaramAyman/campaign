package udacity.com.myapplication;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Items implements Parcelable {
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("budget")
    @Expose
    private Double budget;
    @SerializedName("goal")
    @Expose
    private String goal;
    @SerializedName("category")
    @Expose
    private String category;
    public final static Creator<Items> CREATOR = new Creator<Items>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Items createFromParcel(Parcel in) {
            return new Items(in);
        }

        public Items[] newArray(int size) {
            return (new Items[size]);
        }

    };

    protected Items(Parcel in) {
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.country = ((String) in.readValue((String.class.getClassLoader())));
        this.budget = ((Double) in.readValue((Double.class.getClassLoader())));
        this.goal = ((String) in.readValue((String.class.getClassLoader())));
        this.category = ((String) in.readValue((String.class.getClassLoader())));

    }

    public Items() {
    }
    public String getname() {
        return name;
    }
    public String getcountry() {
        return country;
    }
    public String getGoal() {
        return goal;
    }
    public String getCategory() {
        return category;
    }
    public double getbudget() {
        return budget;
    }


    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(name);
        dest.writeValue(country);
        dest.writeValue(budget);
        dest.writeValue(goal);
        dest.writeValue(category);

    }

    public int describeContents() {
        return 0;
    }

}
