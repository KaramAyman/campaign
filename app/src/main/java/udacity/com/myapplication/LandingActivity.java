package udacity.com.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class LandingActivity extends AppCompatActivity {


    //https://ngkc0vhbrl.execute-api.eu-west-1.amazonaws.com/api/?url=https://arabic.cnn.com/

    private static final String url_For_json =
            "https://ngkc0vhbrl.execute-api.eu-west-1.amazonaws.com/api/?url=";

    TextView nametv, idtv, categorytv;
    Button searchButton, moreButton;
    EditText et;
    String incomingURL;
    private TextView mErrorMessage;
    private ProgressBar mProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landing);

        nametv = (TextView) findViewById(R.id.landing_tv_name);
        idtv = (TextView) findViewById(R.id.landing_tv_);
        categorytv = (TextView) findViewById(R.id.landing_tv_url_category);

        mErrorMessage = (TextView) findViewById(R.id.landing_tv_error_message);
        mProgressBar = (ProgressBar) findViewById(R.id.progressBar_loading_indicator);


        searchButton = (Button) findViewById(R.id.landing_btn_search);
        moreButton = (Button) findViewById(R.id.landing_btn_more);
        moreButton.setVisibility(View.INVISIBLE);
        et = (EditText) findViewById(R.id.landing_et_url);
        moreButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openChartActivity();
            }
        });
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                incomingURL = et.getText().toString();
                loadBakingData();
                hideKeyboard();
                moreButton.setVisibility(View.INVISIBLE);

            }
        });
    }

    private void openChartActivity() {
        Intent mIntent = new Intent(LandingActivity.this, ChartActivity.class);
        startActivity(mIntent);
    }

    public void loadBakingData() {
        hideErrorMessage();
        new FetchMovieTask().execute(url_For_json);
    }

    public void hideErrorMessage() {
        mErrorMessage.setVisibility(View.INVISIBLE);
    }

    public void hideKeyboard() {
        View view = getCurrentFocus();
        if (view != null) {
            InputMethodManager
                    imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private class FetchMovieTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... strings) {
            if (strings.length == 0) {

                return null;
            }
            String url = strings[0];
            URL buildUrl = NetworkUtiles.BuildUrl(url, incomingURL);
            Log.d("fullURL", buildUrl.toString());
            String jsonBakingResponse = null;
            try {
                jsonBakingResponse = NetworkUtiles.getResponseFromHttpUrl(buildUrl);
                return jsonBakingResponse;
            } catch (IOException e) {

                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            mProgressBar.setVisibility(View.INVISIBLE);
            try {
                JSONObject obj = new JSONObject(s);
                JSONObject category = obj.getJSONObject("category");
                if (obj != null) {
                    nametv.setText(category.getString("name"));
                    idtv.setText(category.getString("id"));
                    moreButton.setVisibility(View.VISIBLE);
                    categorytv.setVisibility(View.VISIBLE);
                    mErrorMessage.setVisibility(View.INVISIBLE);
                } else if (obj == null) {
                    moreButton.setVisibility(View.INVISIBLE);
                    categorytv.setVisibility(View.INVISIBLE);
                    mErrorMessage.setVisibility(View.VISIBLE);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
