package udacity.com.myapplication;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;
import android.widget.SeekBar;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.formatter.LargeValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class ChartActivity extends AppCompatActivity implements SeekBar.OnSeekBarChangeListener,
        OnChartValueSelectedListener {
    private BarChart chart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_chart);

        setupChart();
    }

    private void setupChart() {
        chart = findViewById(R.id.chartActivity_barChart);
        chart.setOnChartValueSelectedListener(this);
        chart.getDescription().setEnabled(false);


        // scaling can now only be done on x- and y-axis separately
        chart.setPinchZoom(false);

        chart.setDrawBarShadow(false);

        chart.setDrawGridBackground(false);


        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.RIGHT);
        l.setOrientation(Legend.LegendOrientation.VERTICAL);
        l.setDrawInside(true);
        l.setYOffset(0f);
        l.setXOffset(10f);
        l.setYEntrySpace(0f);
        l.setTextSize(12f);

        XAxis xAxis = chart.getXAxis();
        xAxis.setGranularity(1f);
        xAxis.setCenterAxisLabels(true);
        xAxis.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return String.valueOf((int) value);
            }
        });

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setValueFormatter(new LargeValueFormatter());
        leftAxis.setDrawGridLines(false);
        leftAxis.setSpaceTop(35f);
        leftAxis.setAxisMinimum(0f); // this replaces setStartAtZero(true)

        chart.getAxisRight().setEnabled(false);
        List<Items> mJson = new ArrayList<>();
        String json = null;
        try {
            InputStream is = getAssets().open("text.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, StandardCharsets.UTF_8);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        try {
            JSONObject obj = new JSONObject(json);
            JSONArray m_jArry = obj.getJSONArray("Campaign");

            Gson gson = new Gson();
            Type type = new TypeToken<ArrayList<Items>>() {
            }.getType();
            try {
                mJson = gson.fromJson(String.valueOf(m_jArry), type);
            } catch (Exception e) {
                e.printStackTrace();
            }
            buildDataSet(mJson);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void buildDataSet(List<Items> mDataSetJson) {
        float groupSpace = 0.08f;
        float barSpace = 0.03f; // x4 DataSet
        float barWidth = 0.2f; // x4 DataSet

        HashMap<String, ArrayList<Items>> groupedByCountryHM = new HashMap<>();
        HashMap<String, HashMap<String, ArrayList<BarEntry>>> countryCategoryHM = new HashMap<>();
        HashMap<String, Integer> groupedByCategoryHM = new HashMap<>();

        for (int i = 0; i < mDataSetJson.size(); i++) {
            if (!groupedByCountryHM.containsKey(mDataSetJson.get(i).getcountry()))
                groupedByCountryHM.put(mDataSetJson.get(i).getcountry(), new ArrayList<Items>());
            groupedByCountryHM.get(mDataSetJson.get(i).getcountry()).add(mDataSetJson.get(i));


            if (!groupedByCategoryHM.containsKey(mDataSetJson.get(i).getCategory()))
                groupedByCategoryHM.put(mDataSetJson.get(i).getCategory(), Color.rgb(new Random().nextInt(255) + 1,
                        new Random().nextInt(255) + 1,
                        new Random().nextInt(255) + 1));

        }

        for (Map.Entry<String, ArrayList<Items>> countries : groupedByCountryHM.entrySet()) {
            ArrayList<Items> items = countries.getValue();
            for (Items item : items) {
                if (!countryCategoryHM.containsKey(countries.getKey()))
                    countryCategoryHM.put(countries.getKey(), new HashMap<String, ArrayList<BarEntry>>());
                HashMap<String, ArrayList<BarEntry>> pl = countryCategoryHM.get(countries.getKey());
                if (!pl.containsKey(item.getCategory()))
                    pl.put(item.getCategory(), new ArrayList<BarEntry>());
                pl.get(item.getCategory()).add(new BarEntry(1, Float.parseFloat("" + item.getbudget()), item.getcountry()));
                countryCategoryHM.put(countries.getKey(), pl);
            }

        }

         BarData data = new BarData();
        for (String catName : groupedByCategoryHM.keySet()) {
            ArrayList<BarEntry> mEntry = new ArrayList<>();
            for (Map.Entry<String, HashMap<String, ArrayList<BarEntry>>> categories : countryCategoryHM.entrySet()) {
                if (categories.getValue().get(catName) != null)
                    mEntry.addAll(categories.getValue().get(catName));
            }
            BarDataSet mCurrentBarDataSet = new BarDataSet(mEntry, catName);
            mCurrentBarDataSet.setColor(groupedByCategoryHM.get(catName));
            data.addDataSet(mCurrentBarDataSet);

        }

        data.setValueFormatter(new LargeValueFormatter());

        chart.setData(data);

        // specify the width each bar should have
        chart.getBarData().setBarWidth(barWidth);

        // restrict the x-axis range
        chart.getXAxis().setAxisMinimum(0);

        chart.getXAxis().setAxisMaximum(groupedByCountryHM.size());
        chart.groupBars(0, groupSpace, barSpace);
        chart.invalidate();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {

    }

    @Override
    public void onNothingSelected() {

    }
}
